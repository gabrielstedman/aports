# Maintainer: Jeff Dickey <alpine@mise.jdx.dev>
pkgname=mise
pkgver=2024.2.15
pkgrel=0
pkgdesc="Polyglot runtime and dev tool version manager"
url="https://mise.jdx.dev"
arch="all !s390x !riscv64 !ppc64le" # limited by cargo
license="MIT"
makedepends="cargo bash direnv cargo-auditable openssl-dev"
subpackages="$pkgname-doc"
provides="rtx=$pkgver-r$pkgrel"
replaces="rtx"
source="$pkgname-$pkgver.tar.gz::https://github.com/jdx/mise/archive/refs/tags/v$pkgver.tar.gz"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen --bin mise
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/mise -t "$pkgdir/usr/bin/"
	install -Dm644 README.md docs/*.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
	install -Dm644 "man/man1/$pkgname.1" -t "$pkgdir/usr/share/man/man1"
}

sha512sums="
c8f754446f0ef6f6a18f4fd347d84594ab31593dcee1a2824ba2adcf373083eaa55c18eeaeba3f70e6b69860afd444254883a2b6af107b2a78a04fc955b5aa4b  mise-2024.2.15.tar.gz
"
